package lucraft.mods.whoyougonnacall;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = WhoYouGonnaCall.MODID, name = WhoYouGonnaCall.NAME, version = WhoYouGonnaCall.VERSION)
public class WhoYouGonnaCall {

    public static final String MODID = "whoyougonnacall";
    public static final String NAME = "WhoYouGonnaCall";
    public static final String VERSION = "1.12.2-1.0.0";

    private static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {

    }
}
